module.exports = {

  'Navigate to Home': function(client){
    var home = client.page.home();

    home.navigate()
    .assert.title('Timely');

    client.end();
    
  }
};

/*describe('Nightwatch demo using mocha', function() {

	describe('Navigate to Home', function() {

   before(function(client, done) {
   	
     done();
   });

   after(function(client, done) {
     if (client.sessionId) {
       client.end(function() {
         done();
       });
     } else {
       done();
     }
   });

   afterEach(function(client, done) {
     done();
   });

   beforeEach(function(client, done) {
     done();
   });

   it('Should navigate to the home page', function(client) {
   
   	var home = client.page.home();

   	home.navigate()
    .assert.title('Timely');
   });

 });

});*/