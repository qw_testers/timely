
module.exports = {

  'Assert Login Header': function(client){
      var home = client.page.home();
      var login = client.page.login();

    home.navigate()
    .clickButtonLogin();

    client.pause(2000);

    login
    .verify.title('Timely')
    .expect
    .element('@lblTitle')
    .text
    .to
    .equal('Login to Timely');

    client.end();
    }
};
/*describe('Nightwatch demo using mocha', function() {

	describe('Assert Login Title', function() {

   before(function(client, done) {
   	
     done();
   });

   after(function(client, done) {
     if (client.sessionId) {
       client.end(function() {
         done();
       });
     } else {
       done();
     }
   });

   afterEach(function(client, done) {
     done();
   });

   beforeEach(function(client, done) {
     done();
   });

  it('Should assert login page title', function(client) {
   
  		var home = client.page.home();
  		var login = client.page.login();

	 	home.navigate()
	 	.clickButtonLogin();

	 	client.pause(2000);

	 	login
	 	.verify.title('Timely')
	 	.expect
	 	.element('@lblTitle')
	 	.text
	 	.to
	 	.equal('Login to Timely');
   });

 });

});*/