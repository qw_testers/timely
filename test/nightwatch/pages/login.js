var loginCommands = {
	clickButtonSubmit: function(){
		return this.click('@btnSubmit');
	},
	clickButtonGoogleSignIn: function(){
		return this.click('@btnGoogleSignIn');
	},
	clickButtonBack: function(){
		return this.click('@btnBack');
	}
};

module.exports = {
	url: 'http://localhost:3004/',
	commands: [loginCommands],
	elements: {
		btnSubmit: {
			selector: 'button [type=submit]'
		},
		btnBack: {
			selector: 'a#returnHomeBtn'
		},
		btnGoogleSignIn: {
			selector: 'a.google'
		},
		lblTitle: {
			selector: 'p#title'
		}
	}
};