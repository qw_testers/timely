var homeCommands = {
	clickButtonLogin: function(){
		return this.waitForElementVisible('body',3000)
		.click('@btnLogin');
	},
	clickButtonRegister: function(){
		return this.waitForElementVisible('@btnRegister',3000)
		.click('@btnRegister');
	}
};

module.exports = {
	url: 'http://localhost:3004/',
	commands: [homeCommands],
	elements: {
		btnLogin: {
			selector: 'a#loginBtn'
		},
		btnRegister: {
			selector: 'a#registerBtn'
		}
	}
};