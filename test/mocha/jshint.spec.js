require('mocha-jshint')({
	title: 'JavaScript Syntax Analyzer',
	paths: [
	        'public/',
	        'routes/',
	        'models/',
	        'test/',
	        'utils/',
	        './app.js'
	    ]
});