/// <reference path="../typings/mocha/mocha.d.ts"/>
   //Assertion library-
var expect = require('chai').expect;         //Assertion library-expect
var supertest = require('supertest');
var should = require('should').should;
//var mongoose = require('mongoose');	
var app = require('../../app.js');
var nightwatch = require('nightwatch');

describe('Timely demo using mocha', function() {

	describe('Navigate to Home', function() {

		var client = nightwatch.initClient({
    silent : true
  });

  var browser = client.api();

  this.timeout(99999999);

   before(function() {
   	browser.perform(function() {
      console.log('beforeAll');
    });

   });

   after(function(done) {
      browser.end(function() {
      console.log('afterAll');
    });

    client.start(done);
   });

   afterEach(function() {
     browser.perform(function() {
      console.log('afterEach');
    });
   });

   beforeEach(function(done) {
     browser.perform(function() {
      console.log('beforeEach');
    });

    client.start(done);
   });

   it('Should navigate to the home page', function(client) {
   
   	var home = client.page.home();

   	home.navigate()
    .assert.title('Timely');
   });

 });

});

describe('GET',function(){
	
	describe('homepage and check for valid response',function(){
		it('should return a 200 response', function(done){
			
			supertest(app)
			.get('/')
			.set('Accept','application/json')
			.end(function(err,res){
				res.status.should.equal(200);
				done();
			});
		});
	});	

	describe('userlist and check properties',function(){
		it('should be an object with keys and values', function(done){
			
			supertest(app)
			.get('/users/userlist')
			.set('Accept','application/json')
			//the end object is used to handle the response
			.end(function(error,response){
				//gets the length of the respoonse (array/object)
				//loops through all of the user objects and check if all required properties are present
				for (var i = 0; i < response.body.length; i++) { 
    				expect(response.body[i]).to.have.property("_id");
					expect(response.body[i]._id).not.to.equal(null);
					expect(response.body[i]).to.have.property("username");
					expect(response.body[i].username).not.to.equal(null);
					expect(response.body[i]).to.have.property("firstName");
					expect(response.body[i].firstName).not.to.equal(null);
					expect(response.body[i]).to.have.property("lastName");
					expect(response.body[i].lastName).not.to.equal(null);
					expect(response.body[i]).to.have.property("img");
					expect(response.body[i].img).not.to.equal(null);
				}
				done();
			});
		});

	});
});
