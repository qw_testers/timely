'use strict';

//var express = require('express');
//var router = express.Router();
var passport = require('passport');
var _        = require('underscore');
var User     = require('../models/user');

module.exports = function(app, passport) {
  app.get('/auth/login', function(req, res){
      //console.log((req.flash()).error);
      var msg = (req.flash()).error;

      res.render('login', {err: msg !== undefined ? msg[0] : msg, title: 'Timely' });
  });

  app.post('/auth/login', passport.authenticate('local'), function(err, res) {
    
      res.redirect('/home');
  });

  app.get('/auth/register', function(req, res){

      res.render('register', {title: 'Timely'});
  });

  app.post('/auth/register', function(req, res) {

    if(req.body.confirm === req.body.password){
      var sign_in = [];
      sign_in.push( {date:"",time_in:"",time_out:"" } );  //initilize an array of objects for sing in data

            // s
      var u = new User({
        firstName : req.body.firstName,
        lastName: req.body.lastName,
        username: req.body.username,
        img: req.body.img,
        sign_in_data : sign_in
      });

      User.register(u, req.body.password, function(err, user) {
        if (err) {
          return res.render("register", {err: err.message, title: 'Timely'});
        }

        res.render("login", {msg: 'Registration successful, fill in your email and password above.', title: 'Timely'});
        
      });

    }
    else{
      res.render("register", {err: "Sorry. Passwords entered do not match", title: 'Timely'});
    }

  });

  app.get('/auth/google',
    passport.authenticate('google', { scope: ['profile', 'email']}),
    function(req, res){
      

      // The request will be redirected to Google for authentication, so this
      // function will not be called.
    });
/*console.log(err);
        if(err){
            res.render("login", {err: err, title: 'Timely'});
        }
        else{
          res.redirect('/home');
        }*/
  app.get('/auth/google/callback', 
    passport.authenticate('google', { failureRedirect: '/auth/login' }),
    function(req, res) {
      res.redirect('/home');
    });

  app.get('/auth/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });
};