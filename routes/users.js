var express = require('express');
var router  = express.Router();
var User    = require('../models/user');
var moment  = require('moment');
var _       = require('underscore');

router.get('/userlist', function(req, res) {
    User.find({}, function (err, users) {
        res.json(users);
    });
});

//gets sign in data for current date for a specific user
router.get('/signInData', function(req, res){
    var email = req.query.email;
    var dataFound = {};

    User.findOne({ username: email }, 'sign_in_data', function (err, user) {
        if(user){
          var now = moment().format("D-M-YYYY"); 
          _.each(user.sign_in_data, function(data){
              
              if(data.date === now){
                dataFound = data;
              
              }
          });

          res.send(dataFound);
        }
        else{
          res.status(400).send({msg : 'error' + err});
        }
    });
});

router.put('/insertBlank', function(req, res){
   var email    = req.body.email;
   
   User.update({username: email},  
       { 
          $push: 
            {
                'sign_in_data': {
                    date:"", 
                    time_in:"",
                    time_out:"" }
            } 
       },
       function(err, result){
           
       if(result){
            
            res.status(200);
            res.send();
        }
        else{
            res.status(400).send({msg : 'error' + err});
        }
    
    });
});

router.put('/insertSignIn', function(req, res){
  var email    = req.body.email;
  var datetime = req.body.datetime;
  var date     = moment(datetime).format("D-M-YYYY");
  var time     = moment(datetime).format("h:mm");

  User.update( {username: email, 'sign_in_data.date': ''},   //for signing in, date would be empty
       { $set: 
            {
                'sign_in_data.$.date': date,
                'sign_in_data.$.time_in': time
            } 
       },
       function(err, result){
           
       if(result){
            
            res.status(200);
            res.send({msg: time});
        }
        else{
            res.status(400).send({msg : 'error' + err});
        }
    
    });
});

router.put('/insertSignOut', function(req, res){
      var email    = req.body.email;
      var datetime = req.body.datetime;
      var date     = moment(datetime).format("D-M-YYYY");
      var time     = moment(datetime).format("h:mm");
     
       User.update( {username: email, 'sign_in_data.date': date},   
           { $set: 
                {
                    'sign_in_data.$.time_out': time
                } 
           },
           function(err, result){
               
             if(result){
                  
                  res.status(200);
                  res.send({msg: time});
              }
              else{
                  res.status(400).send({msg : 'error' + err});
              }
          });
      }); 

module.exports = router;
