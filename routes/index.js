'use strict';

var express = require('express');
var router = express.Router();
var request = require('request');

router.get('/home', ensureAuthenticated, function(req, res){
  res.render('home', { user : req.user, title: 'Timely'});
});

router.get('/', function(req, res, next) {
	if (req.isAuthenticated()) { res.redirect('/home'); }
  res.render('index', { title: 'Timely' });
});

router.get('/googleae837479e82efe11.html', function(req,res){
	res.sendFile('googleae837479e82efe11.html');
});

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.render('login', {err: 'You have been looged out', title: 'Timely'});
}

module.exports = router;