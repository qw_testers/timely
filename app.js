'use strict';

//App Initializaton
var express    = require('express');
var app        = express();

//Requiring Third Party Modules
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');
var logger     = require('morgan');
var path       = require('path');
var passport   = require('passport');
var flash      = require('connect-flash');
var http       = require('http').Server(app);
var io         = require('socket.io')(http);
var favicon    = require('serve-favicon');
// You need session to use connect flash
var session    = require('express-session');
var dotenv     = require("dotenv");
dotenv.load();

var mongo_connection = 'mongodb://'+process.env.MONGO_USER+":"+process.env.MONGO_PASSWORD+"@"+process.env.MONGO_HOST+':'+process.env.MONGO_PORT+'/'+process.env.MONGO_DB;//'mongodb://timely-db:47fd2f260c0496564390873e5adbd266@dokku-mongo-timely-db:27017/timely-db';//
//console.log(mongo_connection);
var db = mongoose.connect(mongo_connection);

if (process.env.NODE_ENV === 'development') {
  app.locals.pretty = false;
}

//Middleware

app.use(logger('dev'));
app.use( session({
  saveUninitialized : true,
  secret : 'Secret' ,
  resave : true,
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
// Make our db accessible to our router--------------------->
app.use(function(req,res,next){
    req.db = db;
    next();
});

var authConfig = require('./utils/auth/passport-config')(passport);
var socketConfig = require('./utils/socket/socket-config')(io);
//Initialize Routes
var index      = require('./routes/index');
var auth       = require('./routes/auth')(app, passport);
var users      = require('./routes/users');

//App Set Up
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');



//Requiring Custom Modules

mongoose.connection.once('connected', function() {
	//console.log("\n • Connected To Database")
});

//Setting Routes
app.use('/', index);
app.use('/users', users);
//app.use('/auth', auth);
//app.use('/home', authConfig.ensureAuthenticated, home);

module.exports = app;

http.listen(process.env.PORT, function(){
    //console.log('\n • Timely Running On Port #'+process.env.PORT||'3000');
});
