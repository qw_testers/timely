'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');
var bcrypt = require('bcrypt-nodejs');

var User = new Schema(
	{
	    firstName: String,
	    lastName: String,
		password: String,
	    username: String,
	    img: String,
	    sign_in_data:[{
			date: String,
            time_in: String,
            time_out: String
		}]
	}
);

// methods ======================
// generating a hash
User.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
User.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};


User.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', User); 