'use strict';
var _           = require('underscore');

module.exports  = function(io) { 
	var clients = 0;
	var users   = [];
	io.on('connection', function(socket){

	  	socket.on('disconnect', function () {

		    if(socket.email !== 'undefined'){
		    	io.emit('set_offline', socket.email);
		    }
  		});

  		socket.on('user_sign_in', function(email){
  			users = _.union([email], users);
  			socket.email=email;
  			io.emit('user_sign_in', users);
  		});

  		socket.on('user_sign_out', function(email){
  			users.pop(email);
  			io.emit('set_offline', email);
  		});

  		socket.on('time_in', function(data){
  			io.emit('server_time_in', data);
  		});

  		socket.on('time_out', function(data){
  			io.emit('server_time_out', data);
  		});

	});
};