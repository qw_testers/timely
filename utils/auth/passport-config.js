'use strict';

var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var LocalStrategy = require('passport-local').Strategy;
var User = require('../../models/user');
var dotenv = require("dotenv");
dotenv.load();

module.exports = function(passport) { 

  //passport.use(new LocalStrategy(User.authenticate()));

  // use static serialize and deserialize of model for passport session support
  passport.serializeUser(User.serializeUser());
  passport.deserializeUser(User.deserializeUser());

  // use static authenticate method of model in LocalStrategy

  passport.use(new LocalStrategy(User.authenticate()));

  passport.use(new GoogleStrategy({
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.CALLBACK
    },
    function(accessToken, refreshToken, profile, done) {
      //if(profile._json.domain === 'qualityworkscg.com' )
      //{
        User.findOne({ username: profile.emails[0].value }, function (err, user) {
          if (err)
              return done(err, null);

          if (user) {

              // if a user is found, log them in
              return done(null, user);
          } else {
              // if the user isnt in our database, create a new user
              var newUser          = new User();
              var sign_in = [];
              sign_in.push( {date:"",time_in:"",time_out:"" } );  //initilize an array of objects for sing in data

              // set all of the relevant information
              newUser.firstName    = profile.name.givenName;
              newUser.lastName     = profile.name.familyName;
              newUser.username     = profile.emails[0].value;
              newUser.img          = profile.photos[0].value.substring(0, profile.photos[0].value.lastIndexOf('?'));
              newUser.sign_in_data = sign_in;

              
              
              // save the user
              newUser.save(function(err) {
                  if (err)
                      throw err;
                  return done(null, newUser);
              });
          }
        });
      //}
      //else{
        
       // return done(new Error("Invalid host domain"));
      //}
    }
  ));

  function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.redirect('/login');
  }
};
