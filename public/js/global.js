'use strict';

var socket = io();
var bindSignIn=true;
var bindSignOut=true;

$( document ).ready(function() {

    //2. Setting up users screen

    //loading user list data on page load
    var userListContent = '';
    var tableContent = '';

    //if element exists
    if ($('span.email').length){

        $.getJSON( '/users/userlist', function( data ) {
            // Stick our user data array into a userlist variable in the global object
            //console.log(data);
            var users = data.reverse();

            // For each item in our JSON, add a table row and cells to the content string
            $.each(users, function(user){

                //console.log(user);
                userListContent += '<div class="item">';
                userListContent += '<a class="status ui tag label right">Offline</a>';
                userListContent += '<img class="ui avatar image" src="'+this.img+'">';
                userListContent += '<div class="content">';
                userListContent += '<div class="header">'+this.firstName+' '+this.lastName+'</div>';
                userListContent += '<div class="description">'+this.username+'</div>';
                userListContent += '</div>';
                userListContent += '</div>';

                tableContent    += '<tr> <td class=""> <h4 class="ui image header">';
                tableContent    += '<img class="ui mini rounded image" src="'+this.img+'"/>';
                tableContent    += '<div class="content">'+this.firstName+' '+this.lastName;
                tableContent    += '<div class="sub header">'+this.username+'</div></div>';
                tableContent    += '</h4></div></td>';
                tableContent    += getSignInTime(this.sign_in_data, new Date().toISOString());
                tableContent    += getSignOutTime(this.sign_in_data, new Date().toISOString()) +'</tr>';
            });

            // Inject the whole content string into our existing HTML table for enter hours tab
            $('#userList.relaxed.divided.list').html(userListContent);
            $('#tblSignInSummary tbody').html(tableContent);
        });
    
        //1. Setting up sign up screen -- getting current date's sign in data
        $.getJSON('/users/signInData/', {email: $('span.email').text()})
            .done(function (data) { 
                if($.isEmptyObject(data) || data.time_in === ""){
                    //if is empty or sign in time not set we should
                    setSignInSectionEnabled();
                }    
                else if (data.time_out === ""){
                    //if sign in time is set
                    $('#signInTime.value').text(data.time_in);
                    setSignOutSectionEnabled();
                }   
                else{
                    $('#signInTime.value').text(data.time_in);
                    $('#signOutTime.value').text(data.time_out);
                    setAllToDisabled();
                } 
          })

          .fail(function (err) {
              console.log(err);
          });

        socket.emit('user_sign_in', $('span.email').text());

        socket.on('user_sign_in', function(emails){
            setUserOnline(emails);
        });
    }

    socket.on('set_offline', function(email){
        setUserOffline(email);
    });

    socket.on('server_time_in', function(data){
        updateUserSignInTimes(data);
    });

    socket.on('server_time_out', function(data){
        updateUserSignOutTimes(data);
    });

    $('.ui.secondary.pointing.menu .item').tab({history:false});
});

function updateUserSignInTimes (data){
    
    $( '.time_in_summary' ).each(function(){
        
        if(data.email === $(this).siblings('td').children('h4').children('.content').children('.sub.header').text()){
            $(this).removeClass('negative');
            $(this).addClass('positive');
            $(this).text(data.time);
        }
    }); 
}


function updateUserSignOutTimes (data){
    
    $( '.time_out_summary' ).each(function(){
        
        if(data.email === $(this).siblings('td').children('h4').children('.content').children('.sub.header').text()){
            $(this).removeClass('negative');
            $(this).addClass('positive');
            $(this).text(data.time);
        }
    }); 
}
function setUserOnline (emails){
    $( 'a.status' ).each(function(){
        if($.inArray($(this).siblings('.content').children('.description').text(), emails) != -1){
            $(this).removeClass('green');
            $(this).addClass('green');
            $(this).text("Online");
        }
    });
}

function setUserOffline(email){
    $( 'a.status' ).each(function(){
        if($(this).siblings('.content').children('.description').text() === email){
            $(this).removeClass('green');
            $(this).text("Offline");
        }
    });
}

function setSignInSectionEnabled(){
    //1. bind statistic label to current clock
    bindSignIn = true;

    //2. fade color of label
    $('#signInTime.value').css("color", "darkgrey");

    //3.make sign out button disabled
    $('#btnSignOut').removeClass("disabled");
    $('#btnSignOut').addClass("disabled");
}

function setSignOutSectionEnabled(){
    //1. bind statistic label to current clock
    bindSignOut = true;
    bindSignIn  = false;    //this should be set to the time that was signed in

    //2. fade color of label
    $('#signOutTime.value').css("color", "darkgrey");
    $('#signInTime.value').css("color", "black");   //set to black because it is lready saved

    //3.make sign in button disabled
    $('#btnSignOut').removeClass("disabled");
    $('#btnSignIn').removeClass("disabled");
    $('#btnSignIn').addClass("disabled");
}

function setAllToDisabled(){
    //1. bind statistic label to current clock
    bindSignOut = false;
    bindSignIn  = false;    //this should be set to the time that was signed in

    //2. fade color of label
    $('#signOutTime.value').css("color", "black");
    $('#signInTime.value').css("color", "black");   //set to black because it is lready saved

    //3.make sign in button disabled
    $('#btnSignOut').addClass("disabled");
    $('#btnSignIn' ).addClass("disabled");

    $('#statusMsg').html("work in progress...");//getStatusMsg(0,0));
}

function getSignInTime(sign_in_data, selectedDate){
    var date = moment(selectedDate).format("D-M-YYYY");
    var time = "N/A";

    $.each(sign_in_data, function(key, entry){
       
        if(entry.date === date){
            time = entry.time_in;
            return false;
        }
    });

    if(time === "N/A"){
        time = '<td class="time_in_summary negative"> N/A </td>';
    }
    else{
        time = '<td class="time_in_summary positive"> '+time+'</td>';
    }
    return time;
}

function getSignOutTime(sign_in_data, selectedDate){
    var date = moment(selectedDate).format("D-M-YYYY");
    var time = "N/A";

    $.each(sign_in_data, function(key, entry){
       
        if(entry.date === date){
            time = entry.time_out;
            return false;
        }
    });

    if(time === "N/A" || time === ""){
        time = '<td class="time_out_summary negative"> N/A </td>';
    }
    else{
        time = '<td class="time_out_summary positive"> '+time+'</td>';
    }
    return time;
}

$("#chat-menu").on("click", function(){
    $('.ui.sidebar').sidebar('toggle');
});
//functions to handle animation of main 
//window when clicking on a button
$( "#loginBtn" ).on( "click", function() {
	$( '.bounceInLeft' ).addClass('bounceOutRight');
	setTimeout(function() {
		window.location.href = "/auth/login";
	}, 1000);
});

$( "#registerBtn" ).on( "click", function() {
	$( '.bounceInLeft' ).addClass('bounceOutRight');
  	setTimeout(function() {
		window.location.href = "/auth/register";
	}, 1000);
});

$( "#returnHomeBtn" ).on( "click", function() {
	$( '.bounceInLeft' ).addClass('bounceOutRight');
  	setTimeout(function() {
		window.location.href = "/";
	}, 1000);
});

$( "#btnLogout" ).on( "click", function() {
    $('#modalLogout').modal({
        onApprove: function() {
            socket.emit('user_sign_out', $('span.email').text());
            window.location.replace("/auth/logout");
        }
    }).modal('show');
});

$( "#tabUsers" ).on( "click", function(){
});

$("#btnSignIn").on("click", function(){
    $('#modalSignIn').modal({
        onApprove: function() {
            $.ajax({
                type: 'PUT',
                data: {
                        email: $('span.email').text(), 
                        datetime: new Date().toISOString()
                      },
                url: '/users/insertSignIn/'
            }).done(function( response, msg, status ) {
                if (status.status == 200) {
                    var time = response.msg;
                    $.ajax({
                            type: 'PUT',
                            data :{email: $('span.email').text() },
                            url: '/users/insertBlank/' 
                    }).done(function( response, msg, status ) {
                
                        if (status.status == 200) {

                            bindSignIn=false;
                            socket.emit('time_in', {email: $('span.email').text(), time: time});
                            setSignOutSectionEnabled();
                            $('#signInTime.value').text(time); 
                        }
                        else{
                            console.log(response.msg);
                        }
                    });
                }
                else{
                     console.log(response.msg);
                }
            });
        }
    }).modal('show');
});

$("#btnSignOut").on("click", function(){
    $('#modalSignOut').modal({
        onApprove: function() {
            $.ajax({
                type: 'PUT',
                data: {
                        email: $('span.email').text(), 
                        datetime: new Date().toISOString()
                      },
                url: '/users/insertSignOut/'
            }).done(function( response, msg, status ) {
                if (status.status == 200) {

                    socket.emit('time_out', {email: $('span.email').text(), time: response.msg});
                    setAllToDisabled();
                    $('#signOutTime.value').text(response.msg); 
                }
                else{
                    console.log(response.msg);
                }
            });
        }
    }).modal('show');
});

//Shows current javascript time
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    var tod = h>11?"PM":"AM";
    m = checkTime(m);
    s = checkTime(s);

    if($('#time').length){
        $('#time').text( ((h>12)?h-12:h) +":"+m+":"+s+" "+tod);
        
        if(bindSignIn){
            $('#signInTime.value').text( ((h>12)?h-12:h) +":"+m);
            $('#statusMsg').html("work in progress");//getStatusMsg(((h>12)?h-12:h), m));
        }
        
        if(bindSignOut){
            $('#signOutTime.value').text( ((h>12)?h-12:h) +":"+m);
            $('#statusMsg').html("work in progress");//getStatusMsg(((h>12)?h-12:h), m));
        }
    }
    var t = setTimeout(function(){startTime();},500);
}

function getStatusMsg(hr, min){
    if(bindSignIn){
        if(hr<8 && min<40){
            return "<span style='color: green'>You are very early</span>";
        }
        else if(hr<8 && min>39 && min<55){
            return "<span style='color: green'>You are kinda early</span>";
        }
        else if((hr==8 && min>54) || (hr==9 && min<11)){
            return "<span style='color: green'>You are on time</span>";
        }
        else if (hr>8 && min>10 && min<31){
            return "<span style='color: orange'>You are a bit late</span>";
        }
        else{
            return "<p style='color: red'>You are late</p>";
        }
    }
    else if(bindSignOut){
         if(hr==4 && min<40){
            return "<span style='color: green'>You are leaving very early</span>";
        }
        else if(hr==4 && min>39 && min<55){
            return "<span style='color: green'>You are leaving kinda early</span>";
        }
        else if((hr==4 && min>54) || (hr==5 && min<11)){
            return "<span style='color: green'>You are leaving on time</span>";
        }
        else if (hr==5 && min>10 && min<31){
            return "<span style='color: orange'>You are leaving a bit late</span>";
        }
        else{
            return "<span style='color: red'>You are leaving very late... overtime you seh</span>";
        }
    }
    else{
        return "<span style='color: green'>Enjoy the rest of your day</span>";
    }
}

function checkTime(i) {
    if (i<10) {i = "0" + i;}  // add zero in front of numbers < 10
    return i;
}

//registration form validation
var createAllErrors = function() {
    var form = $( this );
    var errorList = $( "ul.errorMessages", form );

    var showAllErrorMessages = function() {
        errorList.empty();
        //hide message area

        $('.message').addClass('hidden');
        $('#registerForm').removeClass('error');
        $('#loginForm').removeClass('error');
        $('.message .header' ).empty();

        // Find all invalid fields within the form.
        var invalidFields = form.find( ":invalid" ).each( function( index, node ) {

            // Find the field's corresponding label
            var label = $(node).attr("placeholder");

                // Opera incorrectly does not fill the validationMessage property.
            var message = node.validationMessage || 'Invalid value.';

            errorList
                .show()
                .append( "<li><b>" + label + "</b> " + message + "</li>" );
        });

        if(invalidFields.length>0){
        	//unhide message area
        	$('.message').removeClass('hidden');
        	$('.message').addClass('error');
        	$('#registerForm').addClass('error');
            $('#loginForm').addClass('error');
        	$('.message .header' ).append( "Double check your data" );
        }
    };

    // Support Safari
    form.on( "submit", function( event ) {
        if ( this.checkValidity && !this.checkValidity() ) {
            $( this ).find( ":invalid" ).first().focus();
            event.preventDefault();
        }
    });

    $( "input[type=submit], button:not([type=button])", form )
        .on( "click", showAllErrorMessages);

    $( "input", form ).on( "keypress", function( event ) {
        var type = $( this ).attr( "type" );
        if ( /date|email|month|number|search|tel|text|time|url|week/.test ( type ) && event.keyCode == 13 ) {
            showAllErrorMessages();
        }
    });
};
  
//for each form submission, call the 
//createAllErrors function  
$( "form" ).each( createAllErrors );
    